using PrismPractice.Core.Mvvm;

namespace PrismPractice.ViewModels.Secondary
{
	public class Secondary1ViewModel : SecondaryViewModelBase
	{
		#region 成员/构造

		public Secondary1ViewModel() : base(nameof(Secondary1ViewModel))
		{
			
		}

		#endregion

		#region 绑定
		#endregion

		#region 命令
		#endregion

		#region 方法
		#endregion
	}
}

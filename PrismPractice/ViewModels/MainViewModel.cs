﻿using PrismPractice.Core.Mvvm;

namespace PrismPractice.ViewModels
{
	public class MainViewModel : RegionViewModelBase
	{
		public MainViewModel() : base(nameof(MainViewModel))
		{
		}
	}
}

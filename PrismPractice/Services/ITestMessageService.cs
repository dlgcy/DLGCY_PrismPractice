﻿namespace PrismPractice.Services
{
	public interface ITestMessageService
	{
		string GetMessage();
	}
}

﻿namespace PrismPractice.Services
{
	public class TestMessageService : ITestMessageService
	{
		public string GetMessage()
		{
			return "Hello from the Test Message Service";
		}
	}
}

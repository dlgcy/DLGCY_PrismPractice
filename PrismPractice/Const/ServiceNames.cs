using PrismPractice.Core.Prism;

namespace PrismPractice.Const
{
	public static class ServiceNames
	{
		public const string MY_DIALOG_SERVICE = nameof(MyDialogService);
	}
}

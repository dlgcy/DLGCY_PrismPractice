﻿using Prism.Events;

namespace PrismPractice.Const
{
	/// <summary>
	/// 进入二级界面的事件（参数为页面名称）
	/// </summary>
	public class EnterSecondaryInterfaceEvent : PubSubEvent<string> { }

	/// <summary>
	/// 返回一级界面的事件
	/// </summary>
	public class ReturnToFirstInterfaceEvent : PubSubEvent { }
}

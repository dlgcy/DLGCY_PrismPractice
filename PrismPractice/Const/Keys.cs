namespace PrismPractice.Const
{
	public static class Keys
	{
		public const string MY_DIALOG_SERVICE_PARA_VM_KEY = "vm";

		public const string DIALOG_PARA_ENDS_WITH_FILTER = "EndsWithFilter";

		public const string DIALOG_PARA_SELECTED_RECIPE_LOCATOR = "SelectedRecipeLocator";
	}
}

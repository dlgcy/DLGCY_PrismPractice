﻿namespace PrismPractice.Const
{
	public static class RegionNames
	{
		public const string ContentRegion = "ContentRegion";
		public const string SecondaryContentRegion = "SecondaryContentRegion";
	}
}

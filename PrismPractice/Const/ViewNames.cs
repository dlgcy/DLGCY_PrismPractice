using PrismPractice.Modules.Test.Views;
using PrismPractice.Views;
using PrismPractice.Views.Secondary;

namespace PrismPractice.Const
{
	public static class ViewNames
	{
		public const string VIEW_A = nameof(ViewA);
		public const string MAIN_VIEW = nameof(MainView);
		public const string CONFIG_VIEW = nameof(ConfigView);
		public const string DATA_LOG_VIEW = nameof(DataLogView);
		public const string LOGIN_VIEW = nameof(LoginView);
		public const string USER_VIEW = nameof(UserView);
		public const string UTILITY_VIEW = nameof(UtilityView);

		public const string SECONDARY_1 = nameof(Secondary1View);
	}
}

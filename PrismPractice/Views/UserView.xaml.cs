﻿using System.Windows.Controls;

namespace PrismPractice.Views
{
	/// <summary>
	/// Interaction logic for UserView
	/// </summary>
	public partial class UserView : UserControl
	{
		public UserView()
		{
			InitializeComponent();
		}
	}
}

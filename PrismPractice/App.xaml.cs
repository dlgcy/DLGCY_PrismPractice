﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Services.Dialogs;
using PrismPractice.Views;
using System.Windows;
using PrismPractice.Const;
using PrismPractice.Core.Prism;
using PrismPractice.Modules.Test;
using PrismPractice.Services;
using PrismPractice.Views.Secondary;

namespace PrismPractice
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

		protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
			//注册服务
			containerRegistry.RegisterSingleton<ITestMessageService, TestMessageService>();
			containerRegistry.RegisterSingleton<IDialogService, MyDialogService>(ServiceNames.MY_DIALOG_SERVICE);

			//注册一级导航页面
			containerRegistry.RegisterForNavigation<MainView>();
			containerRegistry.RegisterForNavigation<LoginView>();
			containerRegistry.RegisterForNavigation<ConfigView>();
			containerRegistry.RegisterForNavigation<DataLogView>();
			containerRegistry.RegisterForNavigation<UserView>();
			containerRegistry.RegisterForNavigation<UtilityView>();

			//注册二级导航页面
			containerRegistry.RegisterForNavigation<Secondary1View>();

			//注册 Dialog
		}

		protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
		{
			moduleCatalog.AddModule<TestModule>();
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			//启动时执行的内容;

			base.OnStartup(e); //必须放在最后，不然执行顺序会有问题。
		}
	}
}

using Prism.Regions;
using PrismPractice.Core.Mvvm;
using PrismPractice.Services;

namespace PrismPractice.Modules.Test.ViewModels
{
	public class ViewAViewModel : RegionViewModelBase
	{
		private string _message;
		public string Message
		{
			get => _message;
			set => SetProperty(ref _message, value);
		}

		public ViewAViewModel(ITestMessageService messageService) : base(nameof(ViewAViewModel))
		{
			Message = messageService.GetMessage();
		}

		public override void OnNavigatedTo(NavigationContext navigationContext)
		{
			//do something
		}
	}
}

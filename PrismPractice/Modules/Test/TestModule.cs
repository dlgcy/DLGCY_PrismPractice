﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using PrismPractice.Const;
using PrismPractice.Modules.Test.Views;

namespace PrismPractice.Modules.Test
{
	public class TestModule : IModule
	{
		private readonly IRegionManager _regionManager;

		public TestModule(IRegionManager regionManager)
		{
			_regionManager = regionManager;
		}

		/// <inheritdoc />
		public void OnInitialized(IContainerProvider containerProvider)
		{
			_regionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.VIEW_A);
		}

		/// <inheritdoc />
		public void RegisterTypes(IContainerRegistry containerRegistry)
		{
			containerRegistry.RegisterForNavigation<ViewA>();
		}
	}
}
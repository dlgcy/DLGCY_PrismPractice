﻿using System.Windows.Controls;

namespace PrismPractice.Modules.Test.Views
{
	/// <summary>
	/// Interaction logic for ViewA.xaml
	/// </summary>
	public partial class ViewA : UserControl
	{
		public ViewA()
		{
			InitializeComponent();
		}
	}
}

using System;
using System.ComponentModel;
using System.Windows;
using Prism.Common;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using PrismPractice.Const;

namespace PrismPractice.Core.Prism
{
	/// <summary>
	/// 修改版的 DialogService。弹窗时支持通过参数传递 DataContext（key 为 "vm"）。
	/// </summary>
	public class MyDialogService : DialogService
	{
		private readonly IContainerExtension _containerExtension;

		public MyDialogService(IContainerExtension containerExtension) : base(containerExtension)
		{
			_containerExtension = containerExtension;
		}

		protected override void ConfigureDialogWindowContent(string dialogName, IDialogWindow window, IDialogParameters parameters)
		{
			var content = _containerExtension.Resolve<object>(dialogName);
			if(!(content is FrameworkElement dialogContent))
				throw new NullReferenceException("A dialog's content must be a FrameworkElement");

			//修改点：支持通过参数传递 DataContext（key 为 "vm"）
			bool hasVm = parameters.TryGetValue<IDialogAware>(Keys.MY_DIALOG_SERVICE_PARA_VM_KEY, out IDialogAware vm);
			if(hasVm)
			{
				dialogContent.DataContext = vm;
			}
			else
			{
				//MvvmHelpers.AutowireViewModel(dialogContent); //提示不存在这个方法，所以将其拷贝出来直接使用，后续存在了的话可以改回去。
				AutowireViewModel(dialogContent);
			}

			if(!(dialogContent.DataContext is IDialogAware viewModel))
				throw new NullReferenceException("A dialog's ViewModel must implement the IDialogAware interface");

			ConfigureDialogWindowProperties(window, dialogContent, viewModel);

			MvvmHelpers.ViewAndViewModelAction<IDialogAware>(viewModel, d => d.OnDialogOpened(parameters));
		}

		/// <summary>
		/// Sets the AutoWireViewModel property to true for the <paramref name="viewOrViewModel"/>.
		/// </summary>
		/// <remarks>
		/// The AutoWireViewModel property will only be set to true if the view
		/// is a <see cref="FrameworkElement"/>, the DataContext of the view is null, and
		/// the AutoWireViewModel property of the view is null.
		/// </remarks>
		/// <param name="viewOrViewModel">The View or ViewModel.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void AutowireViewModel(object viewOrViewModel)
		{
			if(viewOrViewModel is FrameworkElement view && view.DataContext is null && ViewModelLocator.GetAutoWireViewModel(view) is null)
			{
				ViewModelLocator.SetAutoWireViewModel(view, true);
			}
		}
	}
}

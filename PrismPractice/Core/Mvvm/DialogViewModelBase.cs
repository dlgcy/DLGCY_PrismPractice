using System;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace PrismPractice.Core.Mvvm
{
	/// <summary>
	/// 对话框 ViewModel 基类
	/// </summary>
	public class DialogViewModelBase : ViewModelBase, IDialogAware
	{
		public DialogViewModelBase(string name) : base(name)
		{
		}

		#region IDialogAware

		/// <inheritdoc />
		public string Title { get; set; } = "Title";

		/// <summary>
		/// 通知对话框关闭
		/// </summary>
		public event Action<IDialogResult> RequestClose;

		/// <inheritdoc />
		public virtual bool CanCloseDialog()
		{
			return true;
		}

		/// <inheritdoc />
		public virtual void OnDialogClosed()
		{
			//在 RequestClose?.Invoke 之后触发，应该是用于清理用途的；
		}

		/// <inheritdoc />
		public virtual void OnDialogOpened(IDialogParameters parameters)
		{
		}

		#endregion

		#region 命令

		#region [命令] 关闭 Dialog
		private DelegateCommand<string> _CloseDialogCmd;
		public DelegateCommand<string> CloseDialogCmd => _CloseDialogCmd ??= new DelegateCommand<string>(ExecuteCloseDialogCmd);
		private void ExecuteCloseDialogCmd(string tag)
		{
			bool isOk = tag == Boolean.TrueString;
			var result = isOk ? ButtonResult.OK : ButtonResult.Cancel;
			IDialogResult dialogResult = new DialogResult(result);
			AddDialogResultParas(dialogResult);

			RequestClose?.Invoke(dialogResult);
		}

		#endregion

		#endregion

		#region 方法

		/// <summary>
		/// 向对话框结果中添加参数（子类可完全重写）
		/// </summary>
		/// <param name="dialogResult"></param>
		protected virtual void AddDialogResultParas(IDialogResult dialogResult)
		{
		}

		#endregion
	}
}


﻿using PropertyChanged;
using WPFTemplateLib.Mvvm;

namespace PrismPractice.Core.Mvvm
{
	/// <summary>
	/// 使用 PropertyChanged.Fody 包来简化属性变动通知的写法（只需要写普通的公共自动属性即可）
	/// </summary>
	[AddINotifyPropertyChangedInterface]
	public class SimpleBindableBase : ObservableObject
	{
	}
}

using System;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using PrismPractice.Const;

namespace PrismPractice.Core.Mvvm
{
	public class RegionViewModelBase : ViewModelBase, IConfirmNavigationRequest
	{
		protected IRegionManager RegionManager { get; private set; }
		protected IEventAggregator EventAggregator { get; private set; }

		public RegionViewModelBase(string name) : base(name)
		{
			RegionManager = ContainerLocator.Current.Resolve<IRegionManager>();
			EventAggregator = ContainerLocator.Current.Resolve<IEventAggregator>();
		}

		public virtual void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
		{
			continuationCallback(true);
		}

		public virtual bool IsNavigationTarget(NavigationContext navigationContext)
		{
			return true;
		}

		public virtual void OnNavigatedFrom(NavigationContext navigationContext)
		{
		}

		public virtual void OnNavigatedTo(NavigationContext navigationContext)
		{
		}

		#region [命令] 进入二级页面
		private DelegateCommand<string> _EnterSecondaryPageCmd;
		public DelegateCommand<string> EnterSecondaryPageCmd => _EnterSecondaryPageCmd ??= new DelegateCommand<string>(ExecuteSwitchPageCmd);
		/// <summary>
		/// 命令执行方法：进入二级页面
		/// </summary>
		/// <param name="viewName">页面名称</param>
		private void ExecuteSwitchPageCmd(string viewName)
		{
			EventAggregator.GetEvent<EnterSecondaryInterfaceEvent>().Publish(viewName);
		}
		#endregion
	}
}

using System;

namespace PrismPractice.Core.Mvvm
{
	/// <summary>
	/// 包含业务或其它框架相关内容的 VM 基类（此处为示例）
	/// </summary>
	public abstract class BusinessBaseViewModel : SimpleBindableBase
	{
		#region 成员构造

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="name">要求必须传入名称</param>
		protected BusinessBaseViewModel(string name)
		{
			Name = name;
			if (string.IsNullOrEmpty(name))
				throw new ArgumentException("Name cannot be empty.", name);
		}

		#endregion

		#region 属性

		/// <summary>
		/// 名称
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// 是否可见
		/// </summary>
		public bool IsVisible { get; set; }

		#endregion
	}
}

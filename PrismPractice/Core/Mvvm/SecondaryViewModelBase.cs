using Prism.Commands;
using PrismPractice.Const;

namespace PrismPractice.Core.Mvvm
{
	/// <summary>
	/// 二级界面的 ViewModel 基类
	/// </summary>
	public class SecondaryViewModelBase : RegionViewModelBase
	{
		public SecondaryViewModelBase(string name) : base(name)
		{
		}

		#region 命令

		#region [命令] 返回
		private DelegateCommand _BackCmd;
		public DelegateCommand BackCmd => _BackCmd ??= new DelegateCommand(ExecuteBackCmd);
		private void ExecuteBackCmd()
		{
			EventAggregator.GetEvent<ReturnToFirstInterfaceEvent>().Publish();
		}
		#endregion

		#endregion
	}
}
